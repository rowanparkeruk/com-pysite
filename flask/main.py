from __future__ import print_function, division
from flask import Flask, render_template, request, g, redirect, url_for
from werkzeug.contrib.fixers import ProxyFix
from datetime import datetime
from collections import OrderedDict


app = Flask(__name__)


@app.before_request
def before_request():
	request.rp_time = datetime.now().strftime("%H:%M:%S")
	request.rp_date = datetime.now().strftime("%d-%b-%y")
	request.rp_year = datetime.now().strftime("%Y")
	pages = [('about', 'About'), ('projects', 'Projects'), ('contact', 'Contact')]
	g.pages = OrderedDict()
	for (path, title) in pages:
		temp = {}
		temp[path] = title
		g.pages.update(temp)

@app.route('/')
def index():
	return redirect(url_for('about'), code=301)

@app.route('/home')
def home():
	return redirect(url_for('about'), code=301)

@app.route('/about')
def about():
	return render_template('about.html')

@app.route('/projects')
def projects():
	return render_template('projects.html')

@app.route('/contact')
def contact():
	return render_template('contact.html')


app.wsgi_app = ProxyFix(app.wsgi_app)
if __name__ == '__main__':
    app.run(debug=True)

